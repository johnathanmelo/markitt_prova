using System;
using System.Collections.Generic;
using System.Text;

namespace Proshot.CommandClient
{
    /// <summary>
    /// The type of commands that you can sent to the server.(Note : These are just some comman types.You should do the desired actions when a command received to the client yourself.)
    /// </summary>
    public enum CommandType
    {
        /// <summary>
        /// This command will sent to the server when an specific client is had been logged in to the server.The metadata of this command is in this format : "ClientIP:ClientNetworkName"
        /// </summary>
        ClientLoginInform,

        /// <summary>
        /// Send a text message to the server.Pass the body of text message as command's Metadata.
        /// </summary>
        Message,

        /// <summary>
        /// QR Code Automatic
        /// </summary>
        QRCodeAuto,

        /// <summary>
        /// QR Code Manual
        /// </summary>
        QRCodeManual,

        /// <summary>
        /// QR Code Toten
        /// </summary>
        QRCodeToten,

        /// <summary>
        /// Toten Client Queue Request
        /// </summary>
        TotenClientQueueRequest,

        /// <summary>
        /// Entrada Client Queue Request
        /// </summary>
        EntradaClientQueueRequest,

        /// <summary>
        /// Result of Code Verification
        /// </summary>
        CodeVerificationResult,

        /// <summary>
        /// Conclusion of Operation
        /// </summary>
        OperationConclusion,

        /// <summary>
        /// Data of Entrada Screen: "produtor,tipo_caf�"
        /// </summary>
        EntryDataPack,

        /// <summary>
        /// Finish Dispatch of a Toten's Client
        /// </summary>
        FinishClientDispatch,

        /// <summary>
        /// Prova Screen: Request Status One Coffee (Waiting Area)
        /// </summary>
        ProvaRequestCoffeeStatusOne,

        /// <summary>
        /// Data of Prova Screen: "bag,primeira_prova,segunda_prova"
        /// </summary>
        ProvaDataPack,

        /// <summary>
        /// Venda Client Queue Request
        /// </summary>
        VendaClientQueueRequest
    }
}
