﻿using Proshot.CommandClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace markitt_prova
{
    public partial class MainWindow : Form
    {
        String incomingMessage;
        List<string> listLotes;
        List<string> clientList;

        // Communication
        private CMDClient client;
        IPAddress ipServer = IPAddress.Parse("127.0.0.1");

        public MainWindow()
        {
            InitializeComponent();
            StartLogin();
        }

        private void StartLogin()
        {
            comboBoxClientList.Enabled = false;
            buttonRefresh.Enabled = false;
            checkBoxTipoA.Enabled = false;
            checkBoxTipoB.Enabled = false;
            checkBoxTipoC.Enabled = false;
            textBoxQuantidadeA.Text = "";
            textBoxQuantidadeA.Enabled = false;
            textBoxQuantidadeB.Text = "";
            textBoxQuantidadeB.Enabled = false;
            textBoxQuantidadeC.Text = "";
            textBoxQuantidadeC.Enabled = false;
            buttonSendVenda.Enabled = false;

            this.client = new CMDClient(ipServer, 8000, "Tela de Prova");
            this.client.CommandReceived += new CommandReceivedEventHandler(client_CommandReceived);

            // Start timerLogin
            timerLogin.Enabled = true;
            timerLogin.Interval = 5000;
            timerLogin.Tick += new EventHandler(this.timerLogin_Tick);
        }

        void client_CommandReceived(object sender, CommandEventArgs e)
        {
            switch (e.Command.CommandType)
            {
                case (Proshot.CommandClient.CommandType.ProvaRequestCoffeeStatusOne):
                    this.timerLogin.Enabled = false;
                    incomingMessage = e.Command.MetaData;
                    listLotes = incomingMessage.Split('|').ToList(); // formato da msg: "client,type_coffee,number_of_bags|client,type_coffee,number_of_bags"
                    listLotes.Remove(listLotes.Last());
                    FillProvaComboBox();
                    break;

                case (Proshot.CommandClient.CommandType.VendaClientQueueRequest):
                    timerLogin.Enabled = false;
                    incomingMessage = e.Command.MetaData;
                    clientList = incomingMessage.Split(',').ToList();
                    clientList.Remove(clientList.Last());
                    FillVendaComboBox();
                    break;
            }
        }

        private void FillProvaComboBox()
        {
            comboBoxLotesList.Text = "";
            comboBoxLotesList.Items.Clear();

            foreach (string bt in listLotes)
            {
                List<string> coffeeList = bt.Split(',').ToList();
                string c = coffeeList[0] + "  |  " + coffeeList[1] + "  |  Bags: " + coffeeList[2]; // João  |  Tipo A  |  Bags: 2
                comboBoxLotesList.Items.Add(c);
            }

            comboBoxLotesList.Enabled = true;
            buttonRefresh.Enabled = true;
            radioButtonTypeA.Enabled = true;
            radioButtonTypeB.Enabled = true;
            radioButtonTypeC.Enabled = true;
            buttonSend.Enabled = true;
        }

        private void FillVendaComboBox()
        {
            comboBoxClientList.Text = "";
            comboBoxClientList.Items.Clear();

            foreach (string cli in clientList)
            {
                comboBoxClientList.Items.Add(cli);
            }

            textBoxStatus.Clear();
            checkBoxTipoA.Enabled = true;
            checkBoxTipoB.Enabled = true;
            checkBoxTipoC.Enabled = true;
            comboBoxClientList.Enabled = true;
            buttonRefresh.Enabled = true;
            buttonSendVenda.Enabled = true;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            // checar se todos os itens estão preenchidos
            if ((!string.IsNullOrEmpty(comboBoxLotesList.Text)) &&
                ((radioButtonTypeA.Checked) || (radioButtonTypeB.Checked) || (radioButtonTypeC.Checked)))
            {
                if (this.client.Connected)
                {
                    // monta pacote de dados: "cliente,primeira_prova,segunda_prova"
                    string data = BuildProvaDataPack();

                    // envia dados para servidor
                    this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.ProvaDataPack, ipServer, data));

                    // reseta botões
                    comboBoxLotesList.Text = "Carregando...";
                    comboBoxLotesList.Items.Clear();
                    comboBoxLotesList.Enabled = false;
                    buttonRefresh.Enabled = false;
                    radioButtonTypeA.Enabled = false;
                    radioButtonTypeB.Enabled = false;
                    radioButtonTypeC.Enabled = false;
                    buttonSend.Enabled = false;
                    radioButtonTypeA.Checked = false;
                    radioButtonTypeB.Checked = false;
                    radioButtonTypeC.Checked = false;

                    // mensagem informativa
                    MessageBox.Show("Lote enviado!");                    
                }
                else
                {
                    this.timerLogin.Enabled = true;
                }
            }
        }

        private string BuildProvaDataPack()
        {
            string radioSelected = null;

            if (radioButtonTypeA.Checked)
            {
                radioSelected = "Tipo A";
            }
            else if (radioButtonTypeB.Checked)
            {
                radioSelected = "Tipo B";
            }
            else if (radioButtonTypeC.Checked)
            {
                radioSelected = "Tipo C";
            }

            List<string> inputList = comboBoxLotesList.Text.Split('|').ToList();
            string cli = inputList[0].Remove(inputList[0].Length - 2); // remove os dois ultimos chars da string da bag ('  ')
            string firstProva = inputList[1].Substring(2, inputList[1].Length - 2); // remove os dois primeiros chars da string da primeira prova (' ')
            firstProva = firstProva.Remove(firstProva.Length - 2); //// remove os dois ultimos chars da string da primeira prova (' ')
            string data = cli + "," + firstProva + "," + radioSelected; // "cliente,primeira_prova,segunda_prova"
            return data;
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            if (this.client.Connected == true)
            {
                // envia requisição de lista de bags com status 1 para servidor
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.ProvaRequestCoffeeStatusOne, ipServer, null));
            }
            else
            {
                this.timerLogin.Enabled = true;
            }
        }

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            if (this.client.Connected == false)
            {
                comboBoxLotesList.Text = "Conectando...";
                comboBoxLotesList.Enabled = false;
                buttonRefresh.Enabled = false;
                radioButtonTypeA.Enabled = false;
                radioButtonTypeB.Enabled = false;
                radioButtonTypeC.Enabled = false;
                buttonSend.Enabled = false;

                comboBoxClientList.Enabled = false;
                buttonRefresh.Enabled = false;
                checkBoxTipoA.Enabled = false;
                checkBoxTipoB.Enabled = false;
                checkBoxTipoC.Enabled = false;
                textBoxQuantidadeA.Text = "";
                textBoxQuantidadeA.Enabled = false;
                textBoxQuantidadeB.Text = "";
                textBoxQuantidadeB.Enabled = false;
                textBoxQuantidadeC.Text = "";
                textBoxQuantidadeC.Enabled = false;
                buttonSend.Enabled = false;

                this.client.ConnectToServer();
            }
            else
            {
                // envia requisição de lista de bags com status 1 para servidor
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.ProvaRequestCoffeeStatusOne, ipServer, null));

                // envia requisição de lista de clientes para servidor
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.VendaClientQueueRequest, ipServer, null));
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            this.client.Disconnect();
        }

        private void buttonRefreshVenda_Click(object sender, EventArgs e)
        {
            if (this.client.Connected == true)
            {
                // envia requisição de lista de clientes para servidor
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.VendaClientQueueRequest, ipServer, null));
            }
            else
            {
                this.timerLogin.Enabled = true;
            }
        }

        private void buttonSendVenda_Click(object sender, EventArgs e)
        {
            // checar se todos os itens estão preenchidos
            if ((!string.IsNullOrEmpty(comboBoxClientList.Text)) &&
                ((checkBoxTipoA.Checked) || (checkBoxTipoB.Checked) || (checkBoxTipoC.Checked)))
            {
                if (this.client.Connected)
                {
                    // monta pacote de dados: "produtor,tipo_café"
                    string data = BuildVendaDataPack();

                    // envia dados para servidor
                    this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.EntryDataPack, ipServer, data));

                    // reseta botões
                    comboBoxClientList.Text = "";
                    checkBoxTipoA.Checked = false;
                    checkBoxTipoB.Checked = false;
                    checkBoxTipoC.Checked = false;
                    textBoxQuantidadeA.Text = "";
                    textBoxQuantidadeA.Enabled = false;
                    textBoxQuantidadeB.Text = "";
                    textBoxQuantidadeB.Enabled = false;
                    textBoxQuantidadeC.Text = "";
                    textBoxQuantidadeC.Enabled = false;

                    // mensagem informativa
                    textBoxStatus.Text = "Aguardando confirmação da venda...";
                }
                else
                {
                    this.timerLogin.Enabled = true;
                }
            }
        }

        private string BuildVendaDataPack()
        {
            string data = comboBoxClientList.Text;

            if (checkBoxTipoA.Checked)
            {
                if (textBoxQuantidadeA.Text != "" && Convert.ToInt32(textBoxQuantidadeA.Text) != 0 && FormatValid(textBoxQuantidadeA.Text))
                {
                    data += ",Tipo A," + textBoxQuantidadeA.Text;
                }
            }
            else if (checkBoxTipoB.Checked)
            {
                if (textBoxQuantidadeB.Text != "" && Convert.ToInt32(textBoxQuantidadeB.Text) != 0 && FormatValid(textBoxQuantidadeB.Text))
                {
                    data += ",Tipo B," + textBoxQuantidadeB.Text;
                }
            }
            else if (checkBoxTipoC.Checked)
            {
                if (textBoxQuantidadeC.Text != "" && Convert.ToInt32(textBoxQuantidadeC.Text) != 0 && FormatValid(textBoxQuantidadeC.Text))
                {
                    data += ",Tipo C," + textBoxQuantidadeC.Text;
                }
            }

            return data;
        }

        bool FormatValid(string format)
        {
            string allowableLetters = "0123456789";

            foreach (char c in format)
            {
                // This is using String.Contains for .NET 2 compat.,
                //   hence the requirement for ToString()
                if (!allowableLetters.Contains(c.ToString()))
                    return false;
            }

            return true;
        }

        private void checkBoxTipoA_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTipoA.Checked == true)
            {
                textBoxQuantidadeA.Enabled = true;
                textBoxQuantidadeA.Text = "";
            }
            else
            {
                textBoxQuantidadeA.Enabled = false;
                textBoxQuantidadeA.Text = "";
            }
        }

        private void checkBoxTipoB_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTipoB.Checked == true)
            {
                textBoxQuantidadeB.Enabled = true;
                textBoxQuantidadeB.Text = "";
            }
            else
            {
                textBoxQuantidadeB.Enabled = false;
                textBoxQuantidadeB.Text = "";
            }
        }

        private void checkBoxTipoC_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTipoC.Checked == true)
            {
                textBoxQuantidadeC.Enabled = true;
                textBoxQuantidadeC.Text = "";
            }
            else
            {
                textBoxQuantidadeC.Enabled = false;
                textBoxQuantidadeC.Text = "";
            }
        }
    }
}
