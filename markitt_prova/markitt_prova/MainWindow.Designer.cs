﻿namespace markitt_prova
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.timerLogin = new System.Windows.Forms.Timer(this.components);
            this.labelSelectBag = new System.Windows.Forms.Label();
            this.comboBoxLotesList = new System.Windows.Forms.ComboBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.labelSelectResult = new System.Windows.Forms.Label();
            this.radioButtonTypeA = new System.Windows.Forms.RadioButton();
            this.radioButtonTypeB = new System.Windows.Forms.RadioButton();
            this.radioButtonTypeC = new System.Windows.Forms.RadioButton();
            this.buttonSend = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.AbaProva = new System.Windows.Forms.TabPage();
            this.AbaVenda = new System.Windows.Forms.TabPage();
            this.labelQuantidade = new System.Windows.Forms.Label();
            this.labelTipoCafe = new System.Windows.Forms.Label();
            this.textBoxQuantidadeC = new System.Windows.Forms.TextBox();
            this.textBoxQuantidadeB = new System.Windows.Forms.TextBox();
            this.textBoxQuantidadeA = new System.Windows.Forms.TextBox();
            this.checkBoxTipoC = new System.Windows.Forms.CheckBox();
            this.checkBoxTipoB = new System.Windows.Forms.CheckBox();
            this.checkBoxTipoA = new System.Windows.Forms.CheckBox();
            this.buttonSendVenda = new System.Windows.Forms.Button();
            this.buttonRefreshVenda = new System.Windows.Forms.Button();
            this.comboBoxClientList = new System.Windows.Forms.ComboBox();
            this.labelSelecionarComprador = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.AbaProva.SuspendLayout();
            this.AbaVenda.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSelectBag
            // 
            this.labelSelectBag.AutoSize = true;
            this.labelSelectBag.Location = new System.Drawing.Point(3, 15);
            this.labelSelectBag.Name = "labelSelectBag";
            this.labelSelectBag.Size = new System.Drawing.Size(154, 13);
            this.labelSelectBag.TabIndex = 0;
            this.labelSelectBag.Text = "Selecione o lote a ser provado:";
            // 
            // comboBoxLotesList
            // 
            this.comboBoxLotesList.Enabled = false;
            this.comboBoxLotesList.FormattingEnabled = true;
            this.comboBoxLotesList.Location = new System.Drawing.Point(12, 31);
            this.comboBoxLotesList.Name = "comboBoxLotesList";
            this.comboBoxLotesList.Size = new System.Drawing.Size(301, 21);
            this.comboBoxLotesList.TabIndex = 1;
            this.comboBoxLotesList.Text = "Conectando...";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackgroundImage = global::markitt_prova.Properties.Resources.refresh;
            this.buttonRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRefresh.Enabled = false;
            this.buttonRefresh.Location = new System.Drawing.Point(330, 26);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(30, 30);
            this.buttonRefresh.TabIndex = 2;
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // labelSelectResult
            // 
            this.labelSelectResult.AutoSize = true;
            this.labelSelectResult.Location = new System.Drawing.Point(9, 78);
            this.labelSelectResult.Name = "labelSelectResult";
            this.labelSelectResult.Size = new System.Drawing.Size(157, 13);
            this.labelSelectResult.TabIndex = 3;
            this.labelSelectResult.Text = "Selecione o resultado da prova:";
            // 
            // radioButtonTypeA
            // 
            this.radioButtonTypeA.AutoSize = true;
            this.radioButtonTypeA.Enabled = false;
            this.radioButtonTypeA.Location = new System.Drawing.Point(12, 103);
            this.radioButtonTypeA.Name = "radioButtonTypeA";
            this.radioButtonTypeA.Size = new System.Drawing.Size(56, 17);
            this.radioButtonTypeA.TabIndex = 4;
            this.radioButtonTypeA.TabStop = true;
            this.radioButtonTypeA.Text = "Tipo A";
            this.radioButtonTypeA.UseVisualStyleBackColor = true;
            // 
            // radioButtonTypeB
            // 
            this.radioButtonTypeB.AutoSize = true;
            this.radioButtonTypeB.Enabled = false;
            this.radioButtonTypeB.Location = new System.Drawing.Point(12, 126);
            this.radioButtonTypeB.Name = "radioButtonTypeB";
            this.radioButtonTypeB.Size = new System.Drawing.Size(56, 17);
            this.radioButtonTypeB.TabIndex = 5;
            this.radioButtonTypeB.TabStop = true;
            this.radioButtonTypeB.Text = "Tipo B";
            this.radioButtonTypeB.UseVisualStyleBackColor = true;
            // 
            // radioButtonTypeC
            // 
            this.radioButtonTypeC.AutoSize = true;
            this.radioButtonTypeC.Enabled = false;
            this.radioButtonTypeC.Location = new System.Drawing.Point(12, 149);
            this.radioButtonTypeC.Name = "radioButtonTypeC";
            this.radioButtonTypeC.Size = new System.Drawing.Size(56, 17);
            this.radioButtonTypeC.TabIndex = 6;
            this.radioButtonTypeC.TabStop = true;
            this.radioButtonTypeC.Text = "Tipo C";
            this.radioButtonTypeC.UseVisualStyleBackColor = true;
            // 
            // buttonSend
            // 
            this.buttonSend.Enabled = false;
            this.buttonSend.Location = new System.Drawing.Point(12, 183);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(102, 23);
            this.buttonSend.TabIndex = 7;
            this.buttonSend.Text = "Enviar";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.AbaProva);
            this.tabControl1.Controls.Add(this.AbaVenda);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(485, 272);
            this.tabControl1.TabIndex = 8;
            // 
            // AbaProva
            // 
            this.AbaProva.Controls.Add(this.buttonSend);
            this.AbaProva.Controls.Add(this.radioButtonTypeC);
            this.AbaProva.Controls.Add(this.radioButtonTypeB);
            this.AbaProva.Controls.Add(this.radioButtonTypeA);
            this.AbaProva.Controls.Add(this.labelSelectResult);
            this.AbaProva.Controls.Add(this.buttonRefresh);
            this.AbaProva.Controls.Add(this.comboBoxLotesList);
            this.AbaProva.Controls.Add(this.labelSelectBag);
            this.AbaProva.Location = new System.Drawing.Point(4, 22);
            this.AbaProva.Name = "AbaProva";
            this.AbaProva.Padding = new System.Windows.Forms.Padding(3);
            this.AbaProva.Size = new System.Drawing.Size(477, 246);
            this.AbaProva.TabIndex = 0;
            this.AbaProva.Text = "Prova";
            this.AbaProva.UseVisualStyleBackColor = true;
            // 
            // AbaVenda
            // 
            this.AbaVenda.Controls.Add(this.labelQuantidade);
            this.AbaVenda.Controls.Add(this.labelTipoCafe);
            this.AbaVenda.Controls.Add(this.textBoxQuantidadeC);
            this.AbaVenda.Controls.Add(this.textBoxQuantidadeB);
            this.AbaVenda.Controls.Add(this.textBoxQuantidadeA);
            this.AbaVenda.Controls.Add(this.checkBoxTipoC);
            this.AbaVenda.Controls.Add(this.checkBoxTipoB);
            this.AbaVenda.Controls.Add(this.checkBoxTipoA);
            this.AbaVenda.Controls.Add(this.buttonSendVenda);
            this.AbaVenda.Controls.Add(this.buttonRefreshVenda);
            this.AbaVenda.Controls.Add(this.comboBoxClientList);
            this.AbaVenda.Controls.Add(this.labelSelecionarComprador);
            this.AbaVenda.Controls.Add(this.labelStatus);
            this.AbaVenda.Controls.Add(this.textBoxStatus);
            this.AbaVenda.Location = new System.Drawing.Point(4, 22);
            this.AbaVenda.Name = "AbaVenda";
            this.AbaVenda.Padding = new System.Windows.Forms.Padding(3);
            this.AbaVenda.Size = new System.Drawing.Size(477, 246);
            this.AbaVenda.TabIndex = 1;
            this.AbaVenda.Text = "Venda";
            this.AbaVenda.UseVisualStyleBackColor = true;
            // 
            // labelQuantidade
            // 
            this.labelQuantidade.AutoSize = true;
            this.labelQuantidade.Location = new System.Drawing.Point(205, 122);
            this.labelQuantidade.Name = "labelQuantidade";
            this.labelQuantidade.Size = new System.Drawing.Size(83, 13);
            this.labelQuantidade.TabIndex = 39;
            this.labelQuantidade.Text = "Quantidade (kg)";
            // 
            // labelTipoCafe
            // 
            this.labelTipoCafe.AutoSize = true;
            this.labelTipoCafe.Location = new System.Drawing.Point(95, 122);
            this.labelTipoCafe.Name = "labelTipoCafe";
            this.labelTipoCafe.Size = new System.Drawing.Size(68, 13);
            this.labelTipoCafe.TabIndex = 38;
            this.labelTipoCafe.Text = "Tipo de Café";
            // 
            // textBoxQuantidadeC
            // 
            this.textBoxQuantidadeC.Location = new System.Drawing.Point(208, 187);
            this.textBoxQuantidadeC.Name = "textBoxQuantidadeC";
            this.textBoxQuantidadeC.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantidadeC.TabIndex = 37;
            // 
            // textBoxQuantidadeB
            // 
            this.textBoxQuantidadeB.Location = new System.Drawing.Point(208, 164);
            this.textBoxQuantidadeB.Name = "textBoxQuantidadeB";
            this.textBoxQuantidadeB.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantidadeB.TabIndex = 36;
            // 
            // textBoxQuantidadeA
            // 
            this.textBoxQuantidadeA.Location = new System.Drawing.Point(208, 141);
            this.textBoxQuantidadeA.Name = "textBoxQuantidadeA";
            this.textBoxQuantidadeA.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuantidadeA.TabIndex = 35;
            // 
            // checkBoxTipoC
            // 
            this.checkBoxTipoC.AutoSize = true;
            this.checkBoxTipoC.Location = new System.Drawing.Point(96, 187);
            this.checkBoxTipoC.Name = "checkBoxTipoC";
            this.checkBoxTipoC.Size = new System.Drawing.Size(57, 17);
            this.checkBoxTipoC.TabIndex = 34;
            this.checkBoxTipoC.Text = "Tipo C";
            this.checkBoxTipoC.UseVisualStyleBackColor = true;
            this.checkBoxTipoC.CheckedChanged += new System.EventHandler(this.checkBoxTipoC_CheckedChanged);
            // 
            // checkBoxTipoB
            // 
            this.checkBoxTipoB.AutoSize = true;
            this.checkBoxTipoB.Location = new System.Drawing.Point(96, 164);
            this.checkBoxTipoB.Name = "checkBoxTipoB";
            this.checkBoxTipoB.Size = new System.Drawing.Size(57, 17);
            this.checkBoxTipoB.TabIndex = 33;
            this.checkBoxTipoB.Text = "Tipo B";
            this.checkBoxTipoB.UseVisualStyleBackColor = true;
            this.checkBoxTipoB.CheckedChanged += new System.EventHandler(this.checkBoxTipoB_CheckedChanged);
            // 
            // checkBoxTipoA
            // 
            this.checkBoxTipoA.AutoSize = true;
            this.checkBoxTipoA.Location = new System.Drawing.Point(96, 141);
            this.checkBoxTipoA.Name = "checkBoxTipoA";
            this.checkBoxTipoA.Size = new System.Drawing.Size(57, 17);
            this.checkBoxTipoA.TabIndex = 32;
            this.checkBoxTipoA.Text = "Tipo A";
            this.checkBoxTipoA.UseVisualStyleBackColor = true;
            this.checkBoxTipoA.CheckedChanged += new System.EventHandler(this.checkBoxTipoA_CheckedChanged);
            // 
            // buttonSendVenda
            // 
            this.buttonSendVenda.Enabled = false;
            this.buttonSendVenda.Location = new System.Drawing.Point(98, 216);
            this.buttonSendVenda.Name = "buttonSendVenda";
            this.buttonSendVenda.Size = new System.Drawing.Size(102, 23);
            this.buttonSendVenda.TabIndex = 31;
            this.buttonSendVenda.Text = "Enviar";
            this.buttonSendVenda.UseVisualStyleBackColor = true;
            this.buttonSendVenda.Click += new System.EventHandler(this.buttonSendVenda_Click);
            // 
            // buttonRefreshVenda
            // 
            this.buttonRefreshVenda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonRefreshVenda.BackgroundImage")));
            this.buttonRefreshVenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRefreshVenda.Enabled = false;
            this.buttonRefreshVenda.Location = new System.Drawing.Point(368, 83);
            this.buttonRefreshVenda.Name = "buttonRefreshVenda";
            this.buttonRefreshVenda.Size = new System.Drawing.Size(33, 31);
            this.buttonRefreshVenda.TabIndex = 30;
            this.buttonRefreshVenda.UseVisualStyleBackColor = true;
            this.buttonRefreshVenda.Click += new System.EventHandler(this.buttonRefreshVenda_Click);
            // 
            // comboBoxClientList
            // 
            this.comboBoxClientList.FormattingEnabled = true;
            this.comboBoxClientList.Location = new System.Drawing.Point(98, 89);
            this.comboBoxClientList.Name = "comboBoxClientList";
            this.comboBoxClientList.Size = new System.Drawing.Size(264, 21);
            this.comboBoxClientList.TabIndex = 29;
            // 
            // labelSelecionarComprador
            // 
            this.labelSelecionarComprador.AutoSize = true;
            this.labelSelecionarComprador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelecionarComprador.Location = new System.Drawing.Point(94, 65);
            this.labelSelecionarComprador.Name = "labelSelecionarComprador";
            this.labelSelecionarComprador.Size = new System.Drawing.Size(176, 20);
            this.labelSelecionarComprador.TabIndex = 28;
            this.labelSelecionarComprador.Text = "Selecione o comprador:";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Location = new System.Drawing.Point(233, 4);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(48, 17);
            this.labelStatus.TabIndex = 27;
            this.labelStatus.Text = "Status";
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStatus.Location = new System.Drawing.Point(98, 24);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.ReadOnly = true;
            this.textBoxStatus.Size = new System.Drawing.Size(303, 38);
            this.textBoxStatus.TabIndex = 26;
            this.textBoxStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 272);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainWindow";
            this.Text = "Prova";
            this.tabControl1.ResumeLayout(false);
            this.AbaProva.ResumeLayout(false);
            this.AbaProva.PerformLayout();
            this.AbaVenda.ResumeLayout(false);
            this.AbaVenda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerLogin;
        private System.Windows.Forms.Label labelSelectBag;
        private System.Windows.Forms.ComboBox comboBoxLotesList;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Label labelSelectResult;
        private System.Windows.Forms.RadioButton radioButtonTypeA;
        private System.Windows.Forms.RadioButton radioButtonTypeB;
        private System.Windows.Forms.RadioButton radioButtonTypeC;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage AbaProva;
        private System.Windows.Forms.TabPage AbaVenda;
        private System.Windows.Forms.Label labelQuantidade;
        private System.Windows.Forms.Label labelTipoCafe;
        private System.Windows.Forms.TextBox textBoxQuantidadeC;
        private System.Windows.Forms.TextBox textBoxQuantidadeB;
        private System.Windows.Forms.TextBox textBoxQuantidadeA;
        private System.Windows.Forms.CheckBox checkBoxTipoC;
        private System.Windows.Forms.CheckBox checkBoxTipoB;
        private System.Windows.Forms.CheckBox checkBoxTipoA;
        private System.Windows.Forms.Button buttonSendVenda;
        private System.Windows.Forms.Button buttonRefreshVenda;
        private System.Windows.Forms.ComboBox comboBoxClientList;
        private System.Windows.Forms.Label labelSelecionarComprador;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.TextBox textBoxStatus;
    }
}

